# Social Media templates

This project provides social media templates in .psd form. They were originally intended for use in design workshops.

More details forthcoming.

## Examples

### Facebook
![Facebook Page](/proofs/facebookPage.png)
![Messenger](/proofs/messenger.png)

### Instagram
![Instagram Account](/proofs/instaAccount.png)
![Instagram Post](/proofs/instaPost.png)

### Snapchat
![Snapchat](/proofs/snapChat.png)
![Snapchat image](/proofs/snapStory.png)

### Twitter
![Twitter Profile](/proofs/twitterProfile.png)

### WhatsApp
![WhatsApp](/proofs/whatsapp.png)